<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
	'Chorke' => array($vendorDir . '/chorke/src'),
);
