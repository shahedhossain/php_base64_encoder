<?php

namespace Chorke\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

class ImageEncoderCommand extends Command {

    protected function configure() {   
        $type	= 'data';
        $this->setName("base64")
             ->setDescription("Image to base64 image encoder")
             ->setDefinition(array(
                      new InputOption('type', 't', InputOption::VALUE_OPTIONAL, 'base64 string output type', $type)
                ))
             ->setHelp(<<<EOT

Encode options: [data, tag, url]	
data  :: data:image/gif;base64,__base64data__	
url   :: url(data:image/gif;base64,__base64data__)
tag   :: <img src=\"data:image/gif;base64,__base64data__\"/>
exit  :: For quite application
quite :: For quite application

<info>php encoder data</info>
<info>php encoder url</info>
<info>php encoder img</info>
<info>Email: formative.soft@gmail.comimg</info>

EOT
);
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $header_style = new OutputFormatterStyle('white', 'green', array('bold'));
        $output->getFormatter()->setStyle('header', $header_style);		
		$output->writeln(<<<EOT
<header>
           ###############################################
          ###############################################
         ###                                         ###
        ###  ####### ###### ####### ###### ######## ###
       ###  ##      ##     ##   ## ##       ###    ###
      ###  #####   ###### ##   ## #####    ###    ###
     ###  ##          ## ##   ## ##       ###    ###
    ###  ##   ## ###### ####### ##       ###    ###
   ###                                         ###
  ###############################################
 ###############################################

Encode options: [data, tag, url]	
data  :: data:image/gif;base64,__base64data__	
url   :: url(data:image/gif;base64,__base64data__)
tag   :: <img src=\"data:image/gif;base64,__base64data__\"/>
exit  :: For quit application
quit  :: For quit application
\q    :: For quit application
</header>
EOT
);

        $type 	= $input->getOption('type');
		$type	= in_array($type, array('data', 'tag', 'url')) ? $type : 'data';
		
		while(true){
			$output->writeln('<header>Enter an image path : </header>');
			//$file = $input->getOption('file');
			$output->writeln('<header> ' . $file . ' </header>');
		
		}
    }
}