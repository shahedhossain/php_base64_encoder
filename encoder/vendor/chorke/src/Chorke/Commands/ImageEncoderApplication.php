<?php
namespace Chorke\Commands;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Chorke\Commands\ImageEncoderCommand;

class ImageEncoderApplication extends Application {
  
    protected function getCommandName(InputInterface $input){
        return 'base64';
    }

    protected function getDefaultCommands(){
        $defaultCommands 	= parent::getDefaultCommands();
        $defaultCommands[] = new ImageEncoderCommand();
        return $defaultCommands;
    }

    public function getDefinition() {
        $inputDefinition = parent::getDefinition();
        $inputDefinition->setArguments();
        return $inputDefinition;
    }
}