<?php

namespace Chorke\Validator; 
    
class FileValidator {    
    
	private $file;
	private $isExists;
        
    public function __construct($file) {    
        $this->file 	= $file;
		$this->isExists = $this->isExists();
		if(!$this->isExists){
			echo "\n [" . $file . "]: file not found!\n";
		}
    }

	public function isExists(){
		return file_exists($this->file);
	}
        
    public function isValidExtension(Array $extensions = array('gif', 'png')) {
		if($this->isExists){
			$pathInfo 		= pathinfo($this->file);
			$extension	 	= $pathInfo['extension'];
			return in_array($extension, $extensions);
		}else{
			return false;
		}
        return true;    
    }    
        
    public function isValidMimeContentType(Array $mimeContentTypes = array('image/gif', 'image/png')) {
		if($this->isExists){
			$mimeContentType = mime_content_type($this->file);
			return in_array($mimeContentType, $mimeContentTypes);
		}else{
			return false;
		}
		return true;    
    }
	
	public function validate(Array $extensions = array('gif', 'png'), Array $mimeContentTypes = array('image/gif', 'image/png')){
		return $this->isExists 
		? $this->isValidExtension($extensions) && $this->isValidMimeContentType($mimeContentTypes)
		: false;
	}
		
}