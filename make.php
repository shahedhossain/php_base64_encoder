<?php

define('PREFIX', 'encoder');  
define('PHAR_FILE', PREFIX.'.phar');  
define('DEFAULT_STUB', 'console.php');  
define('BUILD_DIR', __DIR__ . '/main');  
define('INCLUDE_EXTENSION', '/\.php$/');  
  
define('PHAR_TAR_SUFFIX', '.phar.tar');  
define('PHAR_TGZ_SUFFIX', '.phar.tgz');  
define('PHAR_TBZ_SUFFIX', '.phar.tbz');  
define('PHAR_ZIP_SUFFIX', '.phar.zip');  
  
define('PHAR_TAR_FILE', PREFIX . PHAR_TAR_SUFFIX);  
define('PHAR_TGZ_FILE', PREFIX . PHAR_TGZ_SUFFIX);  
define('PHAR_TBZ_FILE', PREFIX . PHAR_TBZ_SUFFIX);  
define('PHAR_ZIP_FILE', PREFIX . PHAR_ZIP_SUFFIX);  
   
try {  
  
 if(file_exists(PHAR_FILE))     unlink(PHAR_FILE);  
 if(file_exists(PHAR_TGZ_FILE)) unlink(PHAR_TGZ_FILE);  
 if(file_exists(PHAR_TBZ_FILE)) unlink(PHAR_TBZ_FILE);  
 if(file_exists(PHAR_ZIP_FILE)) unlink(PHAR_ZIP_FILE);  
   
 /**************************************** 
  * phar file creation  
  ****************************************/  
 $tarphar = new Phar(PHAR_TAR_FILE);  
 $phar = $tarphar->convertToExecutable(Phar::PHAR);  
 $phar->startBuffering();  
 $phar->buildFromDirectory(BUILD_DIR, INCLUDE_EXTENSION);  
 $defaultStub = $phar->createDefaultStub(DEFAULT_STUB);  
 $stub = "#!/usr/bin/php \n".$defaultStub;  
 $phar->setStub($stub);  
 $phar->stopBuffering();  
   
 /**************************************** 
  * phar.tgz file creation  
  ****************************************/  
 $tgz = $phar->convertToExecutable(Phar::TAR, Phar::GZ, PHAR_TGZ_SUFFIX);  
 $tgz->startBuffering();  
 $tgz->setStub($stub);  
 $tgz->stopBuffering();  
   
 /**************************************** 
  * phar.tbz file creation  
  ****************************************/  
 $tbz = $phar->convertToExecutable(Phar::TAR, Phar::BZ2, PHAR_TBZ_SUFFIX);  
 $tbz->startBuffering();  
 $tbz->setStub($stub);  
 $tbz->stopBuffering();  
   
 /***************************************** 
  * phar.zip file creation  
  ****************************************/  
 $zip = $phar->convertToExecutable(Phar::ZIP, Phar::NONE, PHAR_ZIP_SUFFIX);  
 $zip->startBuffering();  
 $zip->setStub($stub);  
 $zip->stopBuffering();  
   
} catch (Exception $e) {  
    echo $e;  
}  