<?php

require_once __DIR__ . '/Chorke/IO/Input.php';    
require_once __DIR__ . '/Chorke/IO/Argument.php';
require_once __DIR__ . '/Chorke/IO/ColorConsole.php';
require_once __DIR__ . '/Chorke/Validator/FileValidator.php'; 
require_once __DIR__ . '/Chorke/Encoder/ImageFileEncoder.php';   

use Chorke\IO\Input;    
use Chorke\IO\Argument;
use Chorke\IO\ColorConsole;
use Chorke\Encoder\ImageFileEncoder;

function printBanner(){
	echo "\r";
	echo "\n";
	echo "           ###############################################\n";
	echo "          ###############################################\n";
	echo "         ###                                         ###\n";
	echo "        ###  ####### ###### ####### ###### ######## ###\n";
	echo "       ###  ##      ##     ##   ## ##       ###    ###\n";
	echo "      ###  #####   ###### ##   ## #####    ###    ###\n";
	echo "     ###  ##          ## ##   ## ##       ###    ###\n";
	echo "    ###  ##   ## ###### ####### ##       ###    ###\n";
	echo "   ###                                         ###\n";
	echo "  ###############################################\n";
	echo " ###############################################\n";
	echo "\n";
	echo "\nEncode options: [data, tag, url] \n";	
	echo "\ndata  :: data:image/gif;base64,__base64data__\n";	
	echo "url   :: url(data:image/gif;base64,__base64data__);\n";	
	echo "tag   :: <img src=\"data:image/gif;base64,__base64data__\"/>\n";
	echo "exit  :: For quite application\n";
	echo "quite :: For quite application\n";
	echo "\q    :: For quite application\n";
}

if (!function_exists('mime_content_type')) {
    function mime_content_type(&$filePath) {
		$mimeContentTypes = array(
			"pdf"=>"application/pdf"
			,"exe"=>"application/octet-stream"
			,"zip"=>"application/zip"
			,"docx"=>"application/msword"
			,"doc"=>"application/msword"
			,"xls"=>"application/vnd.ms-excel"
			,"ppt"=>"application/vnd.ms-powerpoint"
			,"ttf"=>"application/x-font-ttf"
			,"woff"=>"application/font-woff"
			,"gif"=>"image/gif"
			,"png"=>"image/png"
			,"jpeg"=>"image/jpg"
			,"jpg"=>"image/jpg"
			,"mp3"=>"audio/mpeg"
			,"wav"=>"audio/x-wav"
			,"mpeg"=>"video/mpeg"
			,"mpg"=>"video/mpeg"
			,"mpe"=>"video/mpeg"
			,"mov"=>"video/quicktime"
			,"avi"=>"video/x-msvideo"
			,"3gp"=>"video/3gpp"
			,"css"=>"text/css"
			,"jsc"=>"application/javascript"
			,"js"=>"application/javascript"
			,"php"=>"text/html"
			,"htm"=>"text/html"
			,"html"=>"text/html"
        );
		$fileNamePart 	= explode('.',$filePath);
        $extension 		= end($fileNamePart);
        return $mimeContentTypes[strtolower($extension)];
	}
}

if (!function_exists('isPharFileInside')) {
	function isPharFileInside(){
		$isPharFileInside = substr( __dir__ , 0 , 7);
		return $isPharFileInside == 'phar://';
	}
}

if (!function_exists('getPharFileBase')) {
	function getPharFileBase(){
		$getPharFileBase = substr(dirname(__dir__ ), 7);
		return $getPharFileBase;
	}
}

if (!function_exists('getImageFileBase')) {
	function getImageFileBase($file){
		$pharFileBase 	= getPharFileBase();
		if(PHP_OS == 'Windows' || PHP_OS == 'WINNT'){
			$pharFileBase 	= str_replace('/', DIRECTORY_SEPARATOR, $pharFileBase);
		};
		return $pharFileBase . DIRECTORY_SEPARATOR . $file;
	}
}
 
function printEncodedData($file, $option = 'data'){
	$encoder 	= new ImageFileEncoder($file);	
	if($encoder->isEncodable($file)){
		switch($option){
			case 'data': $dataString = $encoder->getHtmlData(); break;
			case 'tag' : $dataString = $encoder->getHtmlTag() ; break;
			case 'url' : $dataString = $encoder->getCssUrl()  ; break;
			default    : $dataString = $encoder->getHtmlData();			
		}
		echo $dataString. "\n\n";
		
		$fontpath = 'glyphicons.font';
		//$fontpath = realpath('glyphicons.font');
		$fontfile = fopen($fontpath, 'w') or die('Unable to open file!');
		fwrite($fontfile, $dataString);
		fclose($fontfile);
	}
}

function readImageFile(){
	$args 		= new Argument();
	$options	= $args->getParams();
	$isSelected = sizeof($options) > 0;
	$option		= $isSelected ? $options[0] : 'data';
	
	if($isSelected){
		echo "Your selected option is ". $option . ".";		
	}else{
		echo "No option selected! System choosed default(data).";
	}
	echo "\n\n\n";

	while(true){
		//$color		= new ColorConsole("Hello World!");
		//echo $color->getBackgroundForeground();
		
		$file 		= Input::readLine("Enter Image Path : ");
		$exitCode	= strtolower($file);
		if($exitCode == 'exit' || $exitCode == 'quit' || $exitCode == '\q'){
			echo "\nEmail: info@chorke.com\n";
			echo "Bye! Thanks for using Chorke Encoder.\n";			
			exit(0);			
		}else{
			if(isPharFileInside()){
				//$file = realpath(getImageFileBase($file));
				$file = realpath($file);
			}
			printEncodedData($file, $option);
		}
	}
}

function bootstrap(){
	printBanner();
	readImageFile();
}

bootstrap();
  