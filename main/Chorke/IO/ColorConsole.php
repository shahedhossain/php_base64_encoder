<?php

namespace Chorke\IO;
    
class ColorConsole {

	private $string;

	static $colors = array(
		'foreground'	=> array(
			'black' 		=> '0;30',
			'darkGray' 		=> '1;30',
			'blue' 			=> '0;34',
			'lightBlue' 	=> '1;34',
			'green' 		=> '0;32',
			'lightGreen'	=> '1;32',
			'cyan' 			=> '0;36',
			'lightCyan'		=> '1;36',
			'red' 			=> '0;31',
			'light_red' 	=> '1;31',
			'purple' 		=> '0;35',
			'lightPurple' 	=> '1;35',
			'brown' 		=> '0;33',
			'yellow' 		=> '1;33',
			'lightGray' 	=> '0;37',
			'white' 		=> '1;37'
		),
		'background'	=> array(
			'black' 		=> '40',
			'red' 			=> '41',
			'green' 		=> '42',
			'yellow' 		=> '43',
			'blue' 			=> '44',
			'magenta' 		=> '45',
			'cyan' 			=> '46',
			'lightGray' 	=> '47'
		)
	);
    
    public function __construct($string) {    
        $this->string = $string;    
    }

	private function getColor($where = 'foreground', $color = 'black'){
		return self::$colors[$where][$color];
	}
	
	private function getBackgroundColor($color = 'lightGray'){
		return $this->getColor('background', $color);
	}
	
	private function getForegroundColor($color = 'white'){
		return $this->getColor('foreground', $color);
	}
       
    public function getBackground($color = 'lightGray') {    
        $colorString 	=  "\033[" . $this->getBackgroundColor($color) . "m";
		$colorString 	= $colorString . $this->string . "\033[0m";
		return $colorString;    
    } 

	public function getForeground($color = 'white') {    
        $colorString 	=  "\033[" . $this->getForegroundColor($color) . "m";
		$colorString 	= $colorString . $this->string . "\033[0m";
		return $colorString;    
    }
	
	public function getBackgroundForeground($background = 'lightGray', $foreground = 'white') {
		$colorString 	= "\033[" . $this->getBackgroundColor($background) . "m";
        $colorString 	= $colorString . "\033[" . $this->getForegroundColor($foreground) . "m";
		$colorString 	= $colorString . $this->string . "\033[0m";
		return $colorString;    
    }
} 